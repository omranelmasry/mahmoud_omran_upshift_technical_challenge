# Development Guide

## Getting Started

#### We mostly use this stack to build our app:

- [React Native](https://reactnative.dev/)

- [Typescript](https://www.typescriptlang.org/docs/)

<hr/>

### What you'll need

- Install React Native Development Environment from [React Native Official Docs Setup](https://reactnative.dev/docs/environment-setup)

  > ensure that you following `React Native CLI Quickstart`

- Clone the APP: 
  https://gitlab.com/omranelmasry/mahmoud_omran_upshift_technical_challenge.git
  or using SSH git@gitlab.com:omranelmasry/mahmoud_omran_upshift_technical_challenge.git

- Run `yarn`

  - if you don't have `yarn` installed, you can install it with `npm install -g yarn`

- For running iOS:

  - run `npx pod-install`
  - run `react-native run-ios`

- For running Android:

  - run `react-native run-android`
