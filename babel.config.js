/** @format */

// prettier-ignore
module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
      [
          'module-resolver',
          {
              root: ['.'],
              extensions: ['.ios.ts', '.android.ts', '.ts', '.ios.tsx', '.android.tsx', '.tsx', '.jsx', '.js', '.json'],
              alias: {
                'actions': './src/redux/actions',
                'reducers': './src/redux/reducers',
                'types': './src/types',
                'redux-store': './src/redux/store.ts',
                'redux-types': './src/redux/types.ts',
                'screens': './src/screens',
                'assets': './src/assets',
                'components': './src/components',
                'utils': './src/utils',
                'API': './src/api',
                'styles': './src/styles',
            },
          },
      ],
      'react-native-reanimated/plugin'
  ],
}
