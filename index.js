/**
 * @format
 */

import React from 'react';
import { AppRegistry } from 'react-native';
import { Router } from './src/navigation'
import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import store from 'redux-store';
import 'react-native-gesture-handler';

const Root = () => (
  <Provider store={store}>
    <SafeAreaProvider>
      <Router />
    </SafeAreaProvider>
  </Provider>
);
AppRegistry.registerComponent(appName, () => Root);
