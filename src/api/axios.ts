/** @format */

import axios from 'axios';
import { REQUEST_MAX_DURATION, REQUEST_TIMEOUT } from './constants';
import { Alert } from 'react-native';

export const mainAxios = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/',
  timeout: REQUEST_MAX_DURATION,
});

// Add a response interceptor
mainAxios.interceptors.response.use(
  (response) => response,
  async (error) => {
    const errorStatus = error?.response?.status;
    const message = error?.response?.data?.error?.message ?? '';

    if (error?.__CANCEL__.getOrElse(false) || errorStatus === REQUEST_TIMEOUT || error?.code.get() === 'ECONNABORTED') {
      Alert.alert('Request Timeout');
      return Promise.reject(error);
    } else if (error.toString() === 'Error: Network Error') {
      Alert.alert('Network Error');
      return Promise.reject(error);
    } else {
      Alert.alert(message);
      return Promise.reject(error);
    }
  }
);

export default { mainAxios };
