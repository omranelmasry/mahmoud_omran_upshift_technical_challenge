/** @format */

export const REQUEST_MAX_DURATION = 20000;
export const REQUEST_TIMEOUT = 408;

export const Constants = {
  posts: {
    getPosts: 'posts',
    create: 'posts',
    getPost: (id: number) => `posts/${id}`,
    updatePost: (id: number) => `posts/${id}`,
    patchPost: (id: number) => `posts/${id}`,
    deletePost: (id: number) => `posts/${id}`,
  },
};

export default Constants;
