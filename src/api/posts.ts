/** @format */

import Requests from './constants';
import { AxiosPromise } from 'axios';
import { mainAxios } from './axios';
import { Post, PostFormInputs } from 'types';

interface GetPostsRequest extends AxiosPromise<Post[]> {}
interface GetPostRequest extends AxiosPromise<Post> {}
interface CreatePostRequest extends AxiosPromise<Post> {}
interface UpdatePostRequest extends AxiosPromise<Post> {}
interface PatchPostRequest extends AxiosPromise<Post> {}
interface DeletePostRequest extends AxiosPromise<void> {}

export const Posts = {
  getPosts: (): GetPostsRequest => mainAxios.get(Requests.posts.getPosts),
  getPost: (id: number): GetPostRequest => mainAxios.get(Requests.posts.getPost(id)),
  createPost: (data: PostFormInputs): CreatePostRequest => mainAxios.post(Requests.posts.create, data),
  updatePost: (data: Post): UpdatePostRequest => mainAxios.put(Requests.posts.updatePost(data.id), data),
  patchPost: (data: Partial<Post>, id: number): PatchPostRequest => mainAxios.patch(Requests.posts.patchPost(id), data),
  deletePost: (id: number): DeletePostRequest => mainAxios.delete(Requests.posts.deletePost(id)),
};
