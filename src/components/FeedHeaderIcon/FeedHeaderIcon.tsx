/** @format */

import { useNavigation } from '@react-navigation/native';
import React, { FunctionComponent } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { styles } from './styles';

export const FeedHeaderIcon: FunctionComponent = () => {
  const navigation = useNavigation();

  const onAddPress = () => {
    navigation.navigate('Create');
  };

  return (
    <TouchableOpacity onPress={onAddPress}>
      <Text style={styles.addIcon}>+</Text>
    </TouchableOpacity>
  );
};
