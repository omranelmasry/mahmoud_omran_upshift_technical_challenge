/** @format */

import { StyleSheet } from 'react-native';
import { Colors } from 'styles';

export const styles = StyleSheet.create({
  addIcon: {
    fontSize: 32,
    color: Colors.blue,
    paddingHorizontal: 16,
  },
});
