/** @format */

import { useNavigation } from '@react-navigation/native';
import React, { Dispatch, FunctionComponent, useState } from 'react';
import { FlatList, SafeAreaView, Text, TouchableOpacity } from 'react-native';
import { useDispatch } from 'react-redux';
import { logout } from 'actions';
import { Route } from 'types';
import { styles } from './styles';

export const DrawerContent: FunctionComponent = () => {
  const navigation = useNavigation();
  const dispatch: Dispatch<any> = useDispatch();

  const routes: Route[] = [
    {
      name: 'Feed',
    },
    {
      name: 'Profile',
    },
  ];

  const [activeRoute, setActiveRoute] = useState('Feed');

  const onRoutePress = (routeName: string) => {
    setActiveRoute(routeName);
    navigation.navigate(routeName);
  };

  const onLogoutPress = async () => {
    await dispatch(logout());
    navigation.reset({
      index: 0,
      routes: [{ name: 'Login' }],
    });
  };

  const renderItem = (route: Route, index: number) => (
    <TouchableOpacity
      onPress={() => onRoutePress(route.name)}
      key={index}
      style={route.name === activeRoute ? styles.selectedRouteRow : styles.unSelectedRouteRow}
    >
      <Text style={styles.routeText}>{route.name}</Text>
    </TouchableOpacity>
  );

  const renderRoutes = () => <FlatList data={routes} renderItem={({ item, index }) => renderItem(item, index)} />;

  const renderLogout = () => (
    <TouchableOpacity onPress={onLogoutPress} style={styles.logoutContainer}>
      <Text style={styles.logoutText}>{'Logout'}</Text>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={styles.container}>
      {renderRoutes()}
      {renderLogout()}
    </SafeAreaView>
  );
};
