/** @format */

import { StyleSheet } from 'react-native';
import { Colors } from 'styles';

export const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: 'space-between' },
  selectedRouteRow: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 44,
    paddingStart: 16,
    backgroundColor: Colors.lightBlue,
  },
  unSelectedRouteRow: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 44,
    paddingStart: 16,
  },
  logoutContainer: {
    height: 44,
    justifyContent: 'center',
    paddingStart: 16,
  },
  routeText: {
    fontSize: 16,
  },
  logoutText: {
    color: Colors.red,
    fontSize: 16,
  },
});
