/** @format */

import React, { FunctionComponent, useEffect } from 'react';
import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import { useForm } from 'react-hook-form';
import { When } from 'utils';
import { styles } from './styles';
import { Post, PostFormInputs } from 'types';

type Props = {
  onPressSubmit: (values: PostFormInputs) => void;
  post?: Post;
};

export const PostForm: FunctionComponent<Props> = ({ onPressSubmit, post }) => {
  const {
    handleSubmit,
    register,
    setValue,
    formState: { errors },
    watch,
  } = useForm<PostFormInputs>();

  useEffect(() => {
    if (post?.id) {
      setValue('body', post.body);
      setValue('title', post.title);
    }
  }, [post, setValue]);

  useEffect(() => {
    register('title', {
      required: {
        value: true,
        message: 'Valid title is required.',
      },
      minLength: {
        value: 4,
        message: 'Body must be at least 4 characters.',
      },
      validate: (value) => value.trim().length >= 4,
    });
    register('body', {
      required: {
        value: true,
        message: 'Body is too short.',
      },
      minLength: {
        value: 20,
        message: 'Body is too short.',
      },
      validate: (value) => value.trim().length >= 20,
    });
  }, [register]);

  return (
    <View style={styles.container}>
      <View style={styles.formContainer}>
        <Text style={styles.formTitle}>{'Title'}</Text>
        <TextInput
          style={styles.textInput}
          value={watch('title')}
          placeholder={'title...'}
          multiline
          onChangeText={(text) => {
            setValue('title', text);
          }}
        />
        <When condition={!!errors.title?.message?.length}>
          <Text style={styles.errorText}>{errors.title?.message}</Text>
        </When>
        <Text style={styles.formTitle}>{'Description'}</Text>
        <TextInput
          style={styles.textInput}
          value={watch('body')}
          multiline
          placeholder={'Body...'}
          onChangeText={(text) => {
            setValue('body', text);
          }}
        />
        <When condition={!!errors.body?.message?.length}>
          <Text style={styles.errorText}>{errors.body?.message}</Text>
        </When>
      </View>
      <TouchableOpacity onPress={handleSubmit(onPressSubmit)} style={styles.submitButton}>
        <Text style={styles.submitText}>{'Submit'}</Text>
      </TouchableOpacity>
    </View>
  );
};
