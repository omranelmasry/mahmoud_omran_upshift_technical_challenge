/** @format */

import { StyleSheet } from 'react-native';
import { Colors } from 'styles';

export const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center' },
  submitButton: {
    marginTop: 32,
    backgroundColor: Colors.blue,
    padding: 16,
    paddingHorizontal: 32,
    borderRadius: 4,
  },
  formContainer: {
    marginTop: 16,
    justifyContent: 'center',
  },
  textInput: {
    marginTop: 8,
    width: 300,
    minHeight: 32,
    borderColor: Colors.black,
    borderWidth: 1,
    paddingStart: 8,
    borderRadius: 4,
  },
  errorText: {
    color: Colors.darkRed,
    marginTop: 8,
  },
  submitText: {
    color: Colors.white,
  },
  formTitle: {
    fontSize: 16,
    fontWeight: '600',
    marginTop: 8,
  },
});
