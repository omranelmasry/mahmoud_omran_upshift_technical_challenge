/** @format */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Create, Feed, Profile, Details, Splash, Login } from 'screens';
import { RootStackParamList } from 'types';
import { DrawerContent, FeedHeaderIcon } from 'components';

const RootStack = createStackNavigator<RootStackParamList>();
const Drawer = createDrawerNavigator();

const DrawerScreens = () => (
  <Drawer.Navigator drawerContent={DrawerContent}>
    <Drawer.Screen
      name="Feed"
      options={{
        headerRight: FeedHeaderIcon,
      }}
      component={Feed}
    />
    <Drawer.Screen name="Profile" component={Profile} />
  </Drawer.Navigator>
);

export function Router() {
  return (
    <NavigationContainer>
      <RootStack.Navigator>
        <RootStack.Screen
          name="Splash"
          component={Splash}
          options={{
            headerShown: false,
          }}
        />
        <RootStack.Screen
          name="Login"
          component={Login}
          options={{
            headerShown: false,
          }}
        />
        <RootStack.Screen
          options={{
            headerShown: false,
          }}
          name="Drawer"
          component={DrawerScreens}
        />
        <RootStack.Screen
          name="Create"
          component={Create}
          options={{
            headerShown: true,
            headerBackTitleVisible: false,
          }}
        />
        <RootStack.Screen
          name="Details"
          component={Details}
          options={{
            headerShown: true,
            headerBackTitleVisible: false,
          }}
        />
      </RootStack.Navigator>
    </NavigationContainer>
  );
}
