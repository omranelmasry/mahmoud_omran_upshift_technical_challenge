/** @format */

import AsyncStorage from '@react-native-async-storage/async-storage';
import { Dispatch } from 'react';
import { CLEAR_USER_DATA, SAVE_USER_DATA } from 'redux-types';
import { User } from 'types';

export const saveUser = (user: User) => {
  return {
    type: SAVE_USER_DATA,
    user,
  };
};

const clearUser = () => {
  return {
    type: CLEAR_USER_DATA,
  };
};

export const login = (user: User) => {
  return async (dispatch: Dispatch<any>) => {
    return new Promise<void>(async (resolve, reject) => {
      try {
        const stringfiedUser = JSON.stringify(user);
        AsyncStorage.setItem('user', stringfiedUser);
        dispatch(saveUser(user));
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  };
};

export const logout = () => {
  return async (dispatch: Dispatch<any>) => {
    return new Promise<void>(async (resolve, reject) => {
      try {
        AsyncStorage.removeItem('user');
        dispatch(clearUser());
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  };
};
