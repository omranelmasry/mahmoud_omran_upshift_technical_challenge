/** @format */

import { Posts } from 'API';
import { Dispatch } from 'react';
import store from 'redux-store';
import { GET_ALL_POSTS } from 'redux-types';
import { Post, PostFormInputs } from 'types';

const saveAllPosts = (posts: Post[]) => {
  return {
    type: GET_ALL_POSTS,
    posts,
  };
};

export const getAllPosts = () => {
  return (dispatch: Dispatch<any>) => {
    return new Promise<void>(async (resolve, reject) => {
      try {
        const { data } = await Posts.getPosts();
        dispatch(saveAllPosts(data));
        resolve();
      } catch (error) {
        reject(error);
        console.log(error);
      }
    });
  };
};

const getUpdatedPostsList = (updatedPost: Post) => {
  const posts: Post[] = store.getState().common.posts,
    updatedPosts = [...posts],
    updatedPostIndex = updatedPosts.findIndex((post) => post.id === updatedPost.id);
  if (updatedPostIndex !== -1) {
    updatedPosts[updatedPostIndex] = updatedPost;
  }
  return updatedPosts;
};

const getUpdatedPostsListAfterAdd = (newPost: Post) => {
  const posts: Post[] = store.getState().common.posts,
    updatedPosts = [...posts];
  updatedPosts.unshift(newPost);
  return updatedPosts;
};

const getUpdatedPostsListAfterDelete = (id: number) => {
  const posts: Post[] = store.getState().common.posts,
    updatedPosts = [...posts],
    deletedPostIndex = updatedPosts.findIndex((post) => post.id === id);
  if (deletedPostIndex !== -1) {
    updatedPosts.splice(deletedPostIndex, 1);
  }
  return updatedPosts;
};

export const updatePost = (post: Post) => {
  return (dispatch: Dispatch<any>) => {
    return new Promise<void>(async (resolve, reject) => {
      try {
        const { data } = await Posts.updatePost(post);
        const updatedList = getUpdatedPostsList(data);
        dispatch(saveAllPosts(updatedList));
        resolve();
      } catch (error) {
        reject(error);
        console.log(error);
      }
    });
  };
};

export const createPost = (post: PostFormInputs) => {
  return (dispatch: Dispatch<any>) => {
    return new Promise<void>(async (resolve, reject) => {
      try {
        const { data } = await Posts.createPost(post);
        const updatedList = getUpdatedPostsListAfterAdd(data);
        dispatch(saveAllPosts(updatedList));
        resolve();
      } catch (error) {
        reject(error);
        console.log(error);
      }
    });
  };
};

export const deletePost = (id: number) => {
  return (dispatch: Dispatch<any>) => {
    return new Promise<void>(async (resolve, reject) => {
      try {
        await Posts.deletePost(id);
        const updatedList = getUpdatedPostsListAfterDelete(id);
        dispatch(saveAllPosts(updatedList));
        resolve();
      } catch (error) {
        reject(error);
        console.log(error);
      }
    });
  };
};
