/** @format */

import { CLEAR_USER_DATA, SAVE_USER_DATA } from 'redux-types';
import { AuthStoreState } from 'types';

export const authInitialState: AuthStoreState = {
  user: {
    name: '',
    email: '',
  },
};

const auth = (state = authInitialState, action: any) => {
  switch (action?.type) {
    case SAVE_USER_DATA:
      return { ...state, user: action.user };
    case CLEAR_USER_DATA:
      return { ...state, user: authInitialState.user };
    default:
      return state;
  }
};

export default auth;
