/** @format */

import { GET_ALL_POSTS } from 'redux-types';
import { CommonStoreState } from 'types';

export const commonInitialState: CommonStoreState = {
  posts: [],
};

const common = (state = commonInitialState, action: any) => {
  switch (action?.type) {
    case GET_ALL_POSTS:
      return { ...state, posts: action.posts };
    default:
      return state;
  }
};

export default common;
