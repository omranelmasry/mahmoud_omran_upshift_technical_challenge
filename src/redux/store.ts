/** @format */

import { applyMiddleware, legacy_createStore as createStore } from 'redux';
import ReduxThunk from 'redux-thunk';
import Reducers from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';

const middlewares = [ReduxThunk];

if (__DEV__) {
  const createDebugger = require('redux-flipper').default;
  middlewares.push(createDebugger());
}

const store = createStore(Reducers, composeWithDevTools(applyMiddleware(...middlewares)));

export default store;
