/** @format */

// ------------------------------------------------------------------------
// Auth types
export const SAVE_USER_DATA = 'SAVE_USER_DATA';
export const CLEAR_USER_DATA = 'CLEAR_USER_DATA';
// ------------------------------------------------------------------------
// Common types
export const GET_ALL_POSTS = 'GET_ALL_POSTS';
