/** @format */

import React, { Dispatch, FunctionComponent } from 'react';
import { createPost } from 'actions';
import { useDispatch } from 'react-redux';
import { CommonStoreState, PostFormInputs } from 'types';
import store from 'redux-store';
import { PostForm } from 'components';

type Props = {
  navigation: any;
};

export const Create: FunctionComponent<Props> = ({ navigation }) => {
  const dispatch: Dispatch<any> = useDispatch();

  const state: CommonStoreState = store.getState().common,
    posts = state.posts;

  const submit = async (values: PostFormInputs) => {
    await dispatch(createPost(values));
    navigation.navigate('Feed', {
      posts,
    });
  };

  return <PostForm onPressSubmit={submit} />;
};
