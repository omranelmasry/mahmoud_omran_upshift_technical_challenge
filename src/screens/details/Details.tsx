/** @format */

import React, { Dispatch, FunctionComponent } from 'react';
import { updatePost } from 'actions';
import { useDispatch } from 'react-redux';
import { CommonStoreState, Post, PostFormInputs } from 'types';
import store from 'redux-store';
import { PostForm } from 'components';

type Props = {
  route: any;
  navigation: any;
};

export const Details: FunctionComponent<Props> = ({ route, navigation }) => {
  const dispatch: Dispatch<any> = useDispatch();

  const state: CommonStoreState = store.getState().common,
    posts = state.posts;
  const post: Post = route?.params?.post;

  const submit = async (values: PostFormInputs) => {
    const updatedPost = { ...post, ...values };
    await dispatch(updatePost(updatedPost));
    navigation.navigate('Feed', {
      posts,
    });
  };

  return <PostForm post={post} onPressSubmit={submit} />;
};
