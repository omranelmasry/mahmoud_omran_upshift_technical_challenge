/** @format */

import React, { Dispatch, FunctionComponent, useEffect } from 'react';
import { useState } from 'react';
import { ActivityIndicator, FlatList, RefreshControl, SafeAreaView, Text, TouchableOpacity, View } from 'react-native';
import { useDispatch } from 'react-redux';
import store from 'redux-store';
import { deletePost, getAllPosts } from 'actions';
import { CommonStoreState, Post } from 'types';
import { Else, If, Then } from 'utils';
import { styles } from './styles';

type Props = {
  navigation: any;
};

export const Feed: FunctionComponent<Props> = ({ navigation }) => {
  const dispatch: Dispatch<any> = useDispatch();
  const state: CommonStoreState = store.getState().common,
    posts = state.posts;

  const [isLoadingPosts, setIsLoadingPosts] = useState(true);
  const [postUndergooingActionId, setPostUndergooingActionId] = useState(0);

  const fetchPosts = async () => {
    try {
      setIsLoadingPosts(true);
      await dispatch(getAllPosts());
      setIsLoadingPosts(false);
    } catch (error) {
      setIsLoadingPosts(false);
      console.log(error);
    }
  };

  useEffect(() => {
    fetchPosts();
  }, []);

  const onPostPress = (post: Post) => {
    navigation.navigate('Details', {
      post,
    });
  };

  const onDeletePress = async (id: number) => {
    try {
      setPostUndergooingActionId(id);
      await dispatch(deletePost(id));
      setPostUndergooingActionId(0);
    } catch (error) {
      setPostUndergooingActionId(0);
      console.log(error);
    }
  };

  const renderPost = (post: Post) => (
    <TouchableOpacity onPress={() => onPostPress(post)} key={post.id} style={styles.postContainer}>
      <If condition={postUndergooingActionId === post.id}>
        <Then>
          <ActivityIndicator style={styles.postLoader} size={'large'} animating />
        </Then>
        <Else>
          <View style={styles.postTitleRow}>
            <Text style={styles.postTitleText}>{post.title}</Text>
            <View style={styles.postButtonsGroup}>
              <TouchableOpacity onPress={() => onDeletePress(post.id)} style={styles.postButton}>
                <Text style={styles.deleteIcon}>X</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => onPostPress(post)} style={styles.postButton}>
                <Text style={styles.editButton}>Edit</Text>
              </TouchableOpacity>
            </View>
          </View>
          <Text style={styles.bodyText}>{post.body}</Text>
        </Else>
      </If>
    </TouchableOpacity>
  );

  const renderPosts = () => (
    <FlatList
      refreshControl={<RefreshControl onRefresh={fetchPosts} refreshing={!!isLoadingPosts} />}
      data={posts}
      renderItem={({ item }) => renderPost(item)}
    />
  );

  return (
    <SafeAreaView>
      <If condition={!!isLoadingPosts}>
        <Then>
          <ActivityIndicator style={styles.loader} size={'large'} animating />
        </Then>
        <Else>{renderPosts()}</Else>
      </If>
    </SafeAreaView>
  );
};
