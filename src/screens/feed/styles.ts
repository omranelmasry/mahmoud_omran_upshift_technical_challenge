/** @format */

import { StyleSheet } from 'react-native';
import { Colors } from 'styles';

export const styles = StyleSheet.create({
  loader: {
    marginTop: 16,
  },
  postLoader: {
    margin: 32,
  },
  postContainer: {
    backgroundColor: Colors.white,
    margin: 8,
    padding: 8,
  },
  postTitleRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  postTitleText: {
    fontSize: 16,
    flex: 0.9,
    fontWeight: '600',
  },
  postButtonsGroup: {
    flex: 0.1,
    alignItems: 'center',
  },
  bodyText: {
    marginTop: 8,
  },
  deleteIcon: {
    fontSize: 16,
    fontWeight: '700',
  },
  postButton: {
    paddingHorizontal: 4,
  },
  editButton: {
    fontWeight: '700',
  },
});
