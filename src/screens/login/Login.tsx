/** @format */

import React, { Dispatch, FunctionComponent, useEffect } from 'react';
import { Image, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { login } from 'actions';
import { useDispatch } from 'react-redux';
import { FormInputs, User } from 'types';
import { styles } from './styles';
import { Images } from 'assets';
import { useForm } from 'react-hook-form';
import { validateEmail, validatePassword, When } from 'utils';

export const Login: FunctionComponent = () => {
  const dispatch: Dispatch<any> = useDispatch();
  const navigation = useNavigation();

  const {
    handleSubmit,
    register,
    setValue,
    formState: { errors },
    watch,
  } = useForm<FormInputs>();

  useEffect(() => {
    register('email', {
      required: {
        value: true,
        message: 'Valid Email is required.',
      },
      validate: (value) => validateEmail(value.trim()),
    });
    register('password', {
      required: {
        value: true,
        message: 'Password must be at least 6 characters.',
      },
      minLength: {
        value: 6,
        message: 'Password must be at least 6 characters.',
      },
      validate: (value) => validatePassword(value.trim()),
    });
  }, [register]);

  const logIn = async (values: FormInputs) => {
    const user: User = {
      email: values.email,
      name: 'Mahmoud Omran',
    };
    await dispatch(login(user));
    navigation.reset({
      index: 0,
      routes: [{ name: 'Drawer' }],
    });
  };

  return (
    <View style={styles.container}>
      <Image source={Images.logo} style={styles.logo} />
      <View style={styles.formContainer}>
        <TextInput
          style={styles.textInput}
          value={watch('email')}
          placeholder={'email...'}
          onChangeText={(text) => {
            setValue('email', text.toLowerCase());
          }}
        />
        <When condition={!!errors.email?.message?.length}>
          <Text style={styles.errorText}>{errors.email?.message}</Text>
        </When>
        <TextInput
          style={styles.textInput}
          value={watch('password')}
          placeholder={'password...'}
          onChangeText={(text) => {
            setValue('password', text);
          }}
        />
        <When condition={!!errors.password?.message?.length}>
          <Text style={styles.errorText}>{errors.password?.message}</Text>
        </When>
      </View>
      <TouchableOpacity onPress={handleSubmit(logIn)} style={styles.loginButton}>
        <Text style={styles.loginText}>{'Login'}</Text>
      </TouchableOpacity>
    </View>
  );
};
