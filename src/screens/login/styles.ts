/** @format */

import { StyleSheet } from 'react-native';
import { Colors } from 'styles';

export const styles = StyleSheet.create({
  container: { flex: 1, justifyContent: 'center', alignItems: 'center' },
  logo: {
    height: 150,
    width: 150,
  },
  loginButton: {
    marginTop: 40,
    backgroundColor: Colors.blue,
    padding: 16,
    paddingHorizontal: 32,
    borderRadius: 4,
  },
  formContainer: {
    marginTop: 32,
    justifyContent: 'center',
  },
  textInput: {
    marginTop: 8,
    minWidth: 300,
    minHeight: 32,
    borderColor: Colors.black,
    borderWidth: 1,
    paddingStart: 8,
    borderRadius: 4,
  },
  errorText: {
    color: Colors.darkRed,
    marginTop: 8,
  },
  loginText: {
    color: Colors.white,
  },
});
