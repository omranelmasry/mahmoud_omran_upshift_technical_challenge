/** @format */

import React, { FunctionComponent } from 'react';
import { Image, Text, View } from 'react-native';
import { styles } from './styles';
import { Images } from 'assets';
import { AuthStoreState } from 'types';
import store from 'redux-store';

export const Profile: FunctionComponent = () => {
  const state: AuthStoreState = store.getState().auth,
    user = state.user;

  return (
    <View style={styles.container}>
      <Image source={Images.profile} style={styles.profilePic} />
      <View style={styles.infoContainer}>
        <Text style={styles.info}>{user.name}</Text>
        <Text style={styles.info}>{user.email}</Text>
      </View>
    </View>
  );
};
