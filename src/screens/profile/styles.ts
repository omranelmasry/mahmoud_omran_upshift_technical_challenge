/** @format */

import { StyleSheet } from 'react-native';
import { Colors } from 'styles';

export const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center', margin: 8, backgroundColor: Colors.white },
  profilePic: {
    marginTop: 40,
    height: 200,
    width: 200,
    borderRadius: 100,
    borderColor: Colors.grey,
    borderWidth: 8,
  },
  infoContainer: {
    marginTop: 20,
    alignItems: 'center',
  },
  info: {
    fontSize: 16,
    marginTop: 8,
    fontWeight: '600',
  },
});
