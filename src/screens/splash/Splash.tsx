/** @format */

import React, { FunctionComponent, useEffect } from 'react';
import { Image, View } from 'react-native';
import { Images } from 'assets';
import { styles } from './styles';
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';
import { User } from 'types';
import { saveUser } from 'actions';
import AsyncStorage from '@react-native-async-storage/async-storage';

type Props = {};

export const Splash: FunctionComponent<Props> = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const checkSavedUser = async () => {
    const user = await AsyncStorage.getItem('user');
    if (user?.length) {
      const parsedUser: User = JSON.parse(user);
      dispatch(saveUser(parsedUser));
    }
    return !!user?.length;
  };

  const reRouteUser = async () => {
    const isUserLoggedIn = await checkSavedUser();
    if (isUserLoggedIn) {
      navigation.reset({
        index: 0,
        routes: [{ name: 'Drawer' }],
      });
    } else {
      navigation.reset({
        index: 0,
        routes: [{ name: 'Login' }],
      });
    }
  };

  useEffect(() => {
    reRouteUser();
  }, []);

  return (
    <View style={styles.container}>
      <Image style={styles.logo} source={Images.logo} />
    </View>
  );
};
