/** @format */

export const Colors = {
  white: '#FFFFFF',
  grey: '#E5E4E2',
  black: '#000000',
  lightBlue: '#7EB6FF',
  blue: '#0096FF',
  red: '#FF5733',
  darkRed: '#FF0000',
};
