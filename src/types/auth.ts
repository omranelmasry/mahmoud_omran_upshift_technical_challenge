/** @format */

export type AuthStoreState = {
  user: User;
};

export type User = {
  name: string;
  email: string;
};

export type FormInputs = {
  email: string;
  password: string;
};
