/** @format */

export type CommonStoreState = {
  posts: Post[];
};

export type Post = {
  id: number;
  title: string;
  body: string;
  userId: number;
};

export type PostFormInputs = {
  title: string;
  body: string;
};
