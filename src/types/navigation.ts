/** @format */

export type RootStackParamList = {
  Splash: undefined;
  Feed: undefined;
  Create: undefined;
  Drawer: undefined;
  Login: undefined;
  Details: undefined;
};

export type Route = {
  name: string;
};
